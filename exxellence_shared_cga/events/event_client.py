# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import httpx
import ssl
from .event_schemas import Event
from loguru import logger
from typing import Union


class EventClient:
    """
    Client to fire a new event to the event-api
    """

    def __init__(self, url: str):
        """
        Create a new eventclient and add the url of event-api
        """
        self._url = url
        # Python 3.10 requires to add ssl.PROTOCOL_TLS_CLIENT so it is required to add context to
        # httpx
        self._context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)

    async def post_event(self, event: Union[Event, dict], access_token: str):
        global context

        post_headers = {
            "content-type": "application/json",
            "authorization": f"Bearer {access_token}",
        }

        event_dict = event.json() if type(event) is Event else event
        logger.debug(event_dict)

        async with httpx.AsyncClient(verify=self._context) as client:
            await client.post(self._url, headers=post_headers, json=event_dict)

        logger.debug("post event done")
