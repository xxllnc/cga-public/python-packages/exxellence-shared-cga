# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import boto3
import botocore
import time
from exxellence_shared_cga.events.event_schemas import Event, EventInfo
from loguru import logger
from mypy_boto3_sns.client import SNSClient
from mypy_boto3_sqs import ServiceResource
from mypy_boto3_sqs.service_resource import Message, Queue
from pydantic import ValidationError


class ServiceException(Exception):
    def __init__(self, code: str, message: str):
        self.code = code
        self.message = message


class HandlerException(Exception):
    def __init__(self, message):
        super().__init__(message)


handlers = dict()


def event_handler(object: str, action: str):
    """
    Decorator that can be used to decorate event handler functions.

    If more than one handler is registerd for the same object/action combination, a
    HandlerException is raised for the second handler encountered.
    """

    event_name = f"{object}{action}"

    def inner_decorator(func):
        event_handler = handlers.get(event_name)
        if event_handler is not None:
            message = f"Handler for {object}:{action} already exists."
            raise HandlerException(message)
        handlers[event_name] = func

        def wrapped(e: EventInfo):
            return func(e)

        return wrapped

    return inner_decorator


class EventListener:
    """
    Can listen for and distribute events. Raises a ServiceException on initialization if failed to
    connect to the configured message bus.
    """

    def __init__(self, config: dict):
        self._config = config
        self.timeout_seconds = 2

    def listen(self):
        """
        Will listen for and distribute events to the registered handlers. Blocks until interrupted
        or terminated.
        """

        queue = self._get_queue(self._config)
        while True:
            messages = queue.receive_messages(MaxNumberOfMessages=1, MessageAttributeNames=["All"])

            if len(messages) > 0:
                for message in messages:
                    if self._handle_message(message):
                        message.delete()
            else:
                time.sleep(self.timeout_seconds)

    def _handle_message(self, message: Message) -> bool:
        handled = False

        try:
            event = Event.parse_raw(message.body)
            handled = self._handle_event(event)
            self._report(message, event, handled)
        except ValidationError:
            logger.warning("Unable to parse message, not in expected format.")
            logger.debug(message.body)

        return handled

    def _handle_event(self, event: Event) -> bool:
        # If a request_id is available, use it to contextualize logging.
        extra = {}
        if event.source.requestId:
            extra["request_id"] = event.source.requestId

        with logger.contextualize(**extra):
            return self._handle_event_with_contextualized_logging(event)

    def _handle_event_with_contextualized_logging(self, event: Event) -> bool:
        handled = False

        event_handler = self._lookup_event_handler(event)
        if event_handler is not None:
            try:
                event_handler(event)
                handled = True
            except Exception as e:
                logger.exception(e)

        return handled

    def _lookup_event_handler(self, event: Event):
        event_name = f"{event.event.object}{event.event.action}"
        event_handler = handlers.get(event_name)

        if event_handler is None:
            event_handler = handlers.get("**")

        return event_handler

    def _report(self, message: Message, event: Event, handled: bool):
        event_name = f"{event.event.object}{event.event.action}"

        if handled:
            logger.debug(f"Message with id {message.message_id} handled as event {event_name}.")
        else:
            logger.warning(
                "Unable to handle message with id {message_id}. No handler found for event "
                + "{event_name}. If maxReceiveCount is reached, the message will be put on the "
                + "dead-message-queue.",
                message_id=message.message_id,
                event_name=event_name,
            )

    def _get_queue(self, config: dict) -> Queue:
        def get_queue() -> Queue:
            client: ServiceResource = boto3.resource(
                "sqs",
                endpoint_url=config["endpoint_url"],
                region_name=config["region_name"],
                aws_secret_access_key=config.get("aws_secret_access_key"),
                aws_access_key_id=config.get("aws_access_key_id"),
                use_ssl=True,
            )
            return client.get_queue_by_name(QueueName=config["sqs_queue_name"])

        return _run_aws_function(get_queue)


class EventDispatcher:
    def __init__(self, config: dict):
        self._config = config

    def fire(self, event: Event) -> str:
        """
        Dispatches the provided event to an Amazon SNS topic.

        Parameters:
        event -- The event to dispatch to the queue.

        Returns:
        str -- The MessageId the event was given.

        Raises:
        ServiceException -- If there is an issue while dispatching the event.
        """

        sns = self._get_sns(self._config)
        message_id = self._publish_event(sns, event)

        return message_id

    def _get_sns(self, config: dict) -> SNSClient:
        return boto3.client(
            "sns",
            endpoint_url=config["endpoint_url"],
            region_name=config["region_name"],
            aws_secret_access_key=config.get("aws_secret_access_key"),
            aws_access_key_id=config.get("aws_access_key_id"),
            use_ssl=True,
        )

    def _publish_event(self, sns: SNSClient, event: Event) -> str:
        def publish_event():
            body = event.model_dump_json()
            topic_arn: str = self._config["event_topic_arn"]

            # A fifo topic ends with .fifo and a MessageGroupId is mandatory
            if topic_arn.endswith(".fifo"):
                response = sns.publish(
                    TopicArn=topic_arn,
                    Message=body,
                    MessageGroupId="xxeco-events",
                )
            else:
                response = sns.publish(TopicArn=topic_arn, Message=body)

            return response["MessageId"]

        return _run_aws_function(publish_event)


def _run_aws_function(func):
    try:
        return func()
    except botocore.exceptions.ClientError as ex:
        code = ex.response["Error"]["Code"]
        message = ex.response["Error"]["Message"]
        raise ServiceException(code, message)
    except botocore.exceptions.ConnectionError as ex:
        code = "ConnectionError"
        message = str(ex)
        raise ServiceException(code, message)
