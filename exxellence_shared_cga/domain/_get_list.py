# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import dateutil.parser
import os
import sqlalchemy as sa
from copy import copy
from exxellence_shared_cga.core.types import (
    FilterOptions,
    RangeOptions,
    SearchResult,
    SortOptions,
    SortOrder,
    UnsignedInt,
)
from loguru import logger
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import distinct
from typing import Optional, Tuple

MAX_PAGE_SIZE = int(os.environ.get("MAX_PAGE_SIZE", "100"))


def get_list(
    db: Session,
    model,
    models,
    query: sa.sql.expression.Select,
    sort_options: Optional[SortOptions] = None,
    range_options: Optional[RangeOptions] = None,
    filter_options: Optional[FilterOptions] = None,
    return_scalars: Optional[bool] = True,
) -> SearchResult:
    """
    Perform a search, following the "Simple REST" API for sorting, filtering
    and paging.
    """

    # First apply filters to list query
    query = __get_list_apply_filter(query, model, models, filter_options)

    # Count total number of results before applying the LIMIT
    total_results = (
        db.execute(
            query.with_only_columns(sa.func.count(distinct(getattr(model, "id", "sid"))))
            .order_by(None)
            .group_by(None)
        )
        .scalars()
        .one()
    )

    # Apply sorting
    query = __get_list_apply_sort(query, model, models, sort_options)

    query, start = __get_list_apply_range(query, range_options)

    logger.debug(query)
    result = db.execute(query).scalars().all() if return_scalars else db.execute(query).all()

    # Last result sequence number = start + number of results - 1
    # (start at 0 + 25 results - 1 = sequence number of last result = 24)
    range_end = start + len(result) - 1

    return SearchResult(
        result=result,
        start=start,
        end=range_end if range_end >= 0 else None,
        total=total_results,
    )


def __get_list_apply_sort(
    query: sa.sql.expression.Select,
    model,
    models,
    sort_options: Optional[SortOptions],
) -> sa.sql.expression.Select:
    if sort_options:
        if sort_options.modelName is not None:
            # Add a join to the query based on the model
            query = query.outerjoin(getattr(model, sort_options.modelName))
            # Now get the model from models and get the field from that model
            sort_column = getattr(
                getattr(models, sort_options.modelName.capitalize()),
                sort_options.field,
            )
            query = query.add_columns(sort_column)
        else:
            try:
                sort_column = getattr(model, sort_options.field)
                sort_column = (
                    sort_column.desc()
                    if sort_options.order == SortOrder.desc
                    else sort_column.asc()
                )
            except:  # noqa E722
                # Add colum as text order
                sort_column = (
                    sa.text(f'"{sort_options.field}" desc')
                    if sort_options.order == SortOrder.desc
                    else sa.text(f'"{sort_options.field}" asc')
                )

    else:
        sort_column = getattr(model, model.default_sort_field, "sid").asc()

    return query.order_by(sort_column)


def __get_list_apply_range(
    query: sa.sql.expression.Select, range_options: Optional[RangeOptions]
) -> Tuple[sa.sql.expression.Select, UnsignedInt]:
    if range_options:
        start = range_options.start
        end = range_options.end
    else:
        start = UnsignedInt(0)
        end = UnsignedInt(MAX_PAGE_SIZE - 1)

    limit = end - start + 1
    offset = start

    query = query.limit(limit).offset(offset)

    return query, start


def __build_fulltext_search(model, models, search_term: str):
    """
    Build a full-text search filter for a given model and search term.

    Will use `ILIKE` and escape .
    """
    search_term = search_term.replace("/", "//").replace("%", "/%").replace("_", "/_")

    fulltext_terms = []
    for item in model.fulltext_fields:
        selected_model = getattr(models, item["modelName"])
        for field in item["fields"]:
            fulltext_terms.append(
                getattr(selected_model, field).ilike(f"%{search_term}%", escape="/")
            )

    return sa.or_(*fulltext_terms)


def _get_filter(k, v, model, models):
    if k == "q":
        if not hasattr(model, "fulltext_fields"):
            raise TypeError("No search fields defined resource")

        return __build_fulltext_search(model, models, str(v))
    elif k[-4:] == "_gte":
        return getattr(model, k[0:-4]) >= dateutil.parser.parse(v)
    elif k[-4:] == "_lte":
        return getattr(model, k[0:-4]) <= dateutil.parser.parse(v)
    else:
        if isinstance(v, list):
            return getattr(model, k).in_(v)
        else:
            return getattr(model, k) == v


def __get_list_apply_filter(
    query: sa.sql.expression.Select,
    model,
    models,
    filter_options: Optional[FilterOptions],
) -> sa.sql.expression.Select:
    """
    Apply the filters specified in `filter_options` to `query` and return the
    new `query`.

    The filters specified are combined with "and" (they all need to match),
    and the values need to match the exactly.

    The "q" filter is special: it performs a full text search (currently
    implemented as an ILIKE) on the "full text" fields specified in the model.

    The "<Date>_gte" and "<Date>_lte" filters are special:
    It filters on the Date and
    checks if the value is greather/less or eaqueal than the value provided

    """
    if not filter_options:
        return query

    options = copy(filter_options.options)

    for k, v in options.items():
        query = query.filter(_get_filter(k, v, model, models))

    return query
