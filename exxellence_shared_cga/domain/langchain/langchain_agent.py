# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2

from exxellence_shared_cga.domain.langchain.langchain_state import State
from langchain_core.runnables import Runnable


class Assistant:
    def __init__(self, assistant_runnable: Runnable):
        self.runnable = assistant_runnable

    def __call__(self, state: State):
        """
        Call the assistant.
        Args:
            state: The current state of the conversation.
        Returns:
            dict: The assistant's response.
        """
        result = self.runnable.invoke(state)

        if not result.tool_calls and (
            not result.content
            or isinstance(result.content, list)
            and not result.content[0].get("text")
        ):
            messages = state["messages"] + [("user", "Respond with a real output.")]
            state = {**state, "messages": messages}  # type: ignore
        return {"messages": result}
