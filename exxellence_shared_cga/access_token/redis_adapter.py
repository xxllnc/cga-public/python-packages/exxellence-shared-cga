# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import redis
from typing import Union


class RedisError(Exception):
    def __init__(self, reason):
        """
        Error indicating that an error occurred while calling Redis.
        """

        self.reason = reason


class RedisAdapter:
    """
    Configurable class for storing and retrieving a JWT access token in Redis
    """

    def __init__(self, host: str, key_name: str, password: Union[str, None] = None):
        """
        Construct a RedisAdapter that returns the JWT access token from the Redis server at the
        specified host stored under the specified key name. The specified password is used to
        connect to the Redis server.
        """

        self.__redis = self._get_redis_instance(host, password)
        self.__key_name = key_name

    def get_access_token(self) -> Union[str, None]:
        """
        Returns the JWT access token from the Redis server.
        """

        return self.__redis.get(self.__key_name)

    def set_access_token(self, access_token: str):
        """
        Adds the specified JWT access token to the Redis server.
        """

        self.__redis.set(self.__key_name, access_token)

    def _get_redis_instance(self, host: str, password: Union[str, None] = None) -> redis.Redis:
        """
        Returns a Redis client for the specified host and password.
        """

        return redis.Redis(host=host, password=password, port=6379, db=0, decode_responses=True)
