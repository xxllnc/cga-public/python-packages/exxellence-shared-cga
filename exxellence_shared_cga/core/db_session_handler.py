# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
from exxellence_shared_cga.access_token.auth0_adapter import Auth0Adapter
from exxellence_shared_cga.core.app_instance_id_handler import AppInstanceIdHandler
from exxellence_shared_cga.core.types import SemanticError, SemanticErrorType
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from typing import Union


class DBSessionHandler:
    """
    Class to handle multi-tenant database sessions
    """

    def __init__(
        self,
        app_id: str,
        controlpanel_url: str,
        sqlalchemy_database_uri: str,
        eco_client_id: str,
        eco_client_secret: str,
        eco_audience: str,
        eco_domain: str,
        redis_host: str,
        redis_password: Union[str, None] = None,
        sqlalchemy_echo: Union[bool, None] = True,
        sqlalchemy_pool_size: Union[int, None] = 5,
        sqlalchemy_pool_max_overflow: Union[int, None] = 10,
        use_app_instance_id: Union[bool, None] = True,
    ):
        """
        Construct an DBSessionHandler that returns the database session for the given
        organization_url.
        """
        self._engines: dict = {}
        self._sessions: dict[str, sessionmaker] = {}
        self._app_id = app_id
        self._controlpanel_url = controlpanel_url
        self._sqlalchemy_database_uri = sqlalchemy_database_uri
        self._auth0 = Auth0Adapter(
            client_id=eco_client_id,
            client_secret=eco_client_secret,
            audience=eco_audience,
            base_url=f"https://{eco_domain}",
            redis_host=redis_host,
            redis_password=redis_password,
        )
        self._sqlalchemy_echo = sqlalchemy_echo
        self._sqlalchemy_pool_size = sqlalchemy_pool_size
        self._sqlalchemy_pool_max_overflow = sqlalchemy_pool_max_overflow
        self._use_app_instance_id = use_app_instance_id
        self._app_instance_id_handler = AppInstanceIdHandler(
            app_id=app_id,
            controlpanel_url=controlpanel_url,
            eco_client_id=eco_client_id,
            eco_client_secret=eco_client_secret,
            eco_audience=eco_audience,
            eco_domain=eco_domain,
            redis_host=redis_host,
            redis_password=redis_password,
        )

    def _raise_not_found_app_instance_id(self):
        raise SemanticError(
            loc={"source": "headers", "field": "organization_url"},
            msg="Error getting app_instance_id",
            type=SemanticErrorType.not_found,
        )

    def _create_new_session(self, organization_url: str):
        suffix = (
            f"_{self._app_instance_id_handler.get_app_instance_id(organization_url)}"
            if self._use_app_instance_id
            else ""
        )

        engine = create_engine(
            url=f"{self._sqlalchemy_database_uri}{suffix}",
            future=True,
            echo=self._sqlalchemy_echo,
            pool_size=self._sqlalchemy_pool_size,
            max_overflow=self._sqlalchemy_pool_max_overflow,
        )

        session = sessionmaker(bind=engine, future=True, autocommit=False, autoflush=False)
        return session

    def get_session(self, organization_url: str):
        session_key = f"{self._app_id}_{organization_url}"
        if session_key not in self._sessions:
            self._sessions[session_key] = self._create_new_session(organization_url)

        return self._sessions[session_key]()
