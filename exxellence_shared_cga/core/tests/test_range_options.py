# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import pytest
from exxellence_shared_cga.core.types import RangeOptions
from pydantic import ValidationError


def test_range_options():
    """
    Test the RangeOptions with a valid start-end range
    """

    range_options = RangeOptions(start=1, end=99)

    assert range_options.check_bounds
    assert range_options.start == 1
    assert range_options.end == 99


def test_range_options_without_bounds_check():
    """
    Test the RangeOptions with a start-end range that exceeds the maximum size of 100. Check bounds
    is false so no ValidationError is thrown.
    """

    RangeOptions(check_bounds=False, start=1, end=102)


def test_range_options_with_wrong_order():
    """
    Test the RangeOptions with a start that is after end. This should result in an ValidationError.
    """

    with pytest.raises(ValidationError):
        RangeOptions(start=101, end=1)


def test_range_options_with_max_size_error():
    """
    Test the RangeOptions with a start-end range that exceeds the maximum size of 100. This should
    result in an ValidationError.
    """

    with pytest.raises(ValidationError):
        RangeOptions(start=1, end=102)
