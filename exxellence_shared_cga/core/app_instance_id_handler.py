# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import requests
from exxellence_shared_cga.access_token.auth0_adapter import Auth0Adapter
from exxellence_shared_cga.core.types import SemanticError, SemanticErrorType
from loguru import logger
from typing import Final, Union


class AppInstanceIdHandler:
    """
    Class to handle app instance id's
    """

    def __init__(
        self,
        app_id: str,
        controlpanel_url: str,
        eco_client_id: str,
        eco_client_secret: str,
        eco_audience: str,
        eco_domain: str,
        redis_host: str,
        redis_password: Union[str, None] = None,
    ):
        """
        Construct the instance id handler to get the correct instance for the given
        organization_url.
        """
        self._app_id = app_id
        self._controlpanel_url = controlpanel_url
        self._auth0 = Auth0Adapter(
            client_id=eco_client_id,
            client_secret=eco_client_secret,
            audience=eco_audience,
            base_url=f"https://{eco_domain}",
            redis_host=redis_host,
            redis_password=redis_password,
        )

    def _raise_not_found_app_instance_id(self):
        raise SemanticError(
            loc={"source": "headers", "field": "organization_url"},
            msg="Error getting app_instance_id",
            type=SemanticErrorType.not_found,
        )

    def get_app_instance_id(self, organization_url: str):
        access_token = self._auth0.get_access_token()

        headers: Final = {
            "content-type": "application/json",
            "authorization": f"Bearer {access_token}",
        }

        params = {"filter": f'{{"appId": "{self._app_id}"}}', "range": "[0,99]"}
        url = f"{self._controlpanel_url}/v1/app_instance"
        logger.debug(f"get app instances. Url: {url} Params: {params} Headers: {headers}")

        response = requests.get(url=url, params=params, headers=headers, verify=True)

        if response.status_code != 200:
            logger.debug(
                f"Error getting app_instance_id: {response.status_code} json: {response.json()}"
            )
            self._raise_not_found_app_instance_id()

        app_instances = response.json()
        logger.debug(f"app_instances: {app_instances}")

        app_instance_id = None
        for app_instance in app_instances:
            if app_instance["primaryHostname"] == organization_url:
                app_instance_id = app_instance["id"].replace("-", "_")
                break

        if app_instance_id is None:
            logger.debug(f"No app_instance_id found in {app_instances} for {organization_url}")
            self._raise_not_found_app_instance_id()

        return app_instance_id
