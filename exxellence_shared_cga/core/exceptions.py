# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
import copy
from exxellence_shared_cga.core.types import (
    ProcessError,
    SemanticError,
    SemanticErrorType,
)
from fastapi import status as http_status
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from typing import Any, Dict, List


class shared_validation_exception_handler:
    def __call__(self, exception) -> JSONResponse:
        """
        Convert default format to desired format where 'loc' is not a tuple but a
        dict: loc: { 'source': 'body', 'field': 'name'}. See CTECO-94
        """

        errors: List[Dict[str, Any]] = copy.deepcopy(exception.errors())

        for i, e in enumerate(exception.errors()):
            loc = e["loc"]

            if isinstance(loc, tuple) and len(loc) == 2:
                new_loc: Dict[str, Any] = {"source": loc[0], "field": loc[1]}
                errors[i]["loc"] = new_loc

        """
        jsonable_encoder already gives a formated error in this format:
        {
        "detail": [
            {
            "loc": {
                "source": "body",
                "field": "contact"
            },
            "msg": "field required",
            "type": "value_error.missing"
            }
        ]
        }
        """

        return JSONResponse(
            status_code=http_status.HTTP_422_UNPROCESSABLE_ENTITY,
            content=jsonable_encoder({"detail": errors}),
        )


class shared_semantic_error_handler:
    def __call__(self, request, exception: SemanticError) -> JSONResponse:
        """
        Handles a SemanticError raised in the domain conform CTECO-97
        """

        status_code_map = {
            SemanticErrorType.already_deleted: http_status.HTTP_409_CONFLICT,
            SemanticErrorType.invalid_input: http_status.HTTP_400_BAD_REQUEST,
            SemanticErrorType.not_found: http_status.HTTP_404_NOT_FOUND,
            SemanticErrorType.not_unique: http_status.HTTP_409_CONFLICT,
            SemanticErrorType.conflict: http_status.HTTP_409_CONFLICT,
        }

        message = exception.msg if isinstance(exception.msg, str) else "unknow error"
        if (
            isinstance(exception.msg, list)
            and len(exception.msg) > 0
            and "msg" in exception.msg[0]
        ):
            message = exception.msg[0]["msg"]

        return JSONResponse(
            status_code=status_code_map[exception.type],
            content={"detail": [exception.__dict__], "message": message},
        )


class shared_process_error_handler:
    def __call__(self, exception: ProcessError) -> JSONResponse:
        message = exception.msg if isinstance(exception.msg, str) else "unknow error"

        if (
            isinstance(exception.msg, list)
            and len(exception.msg) > 0
            and "msg" in exception.msg[0]
        ):
            message = exception.msg[0]["msg"]

        res = JSONResponse(
            status_code=exception.status_code,
            content={"detail": [exception.__dict__], "message": message},
        )
        return res
