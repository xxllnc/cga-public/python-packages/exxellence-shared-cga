# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2
"""
Helper functions for "Simple-REST" API requests

The Simple-REST style of API is defined at
https://github.com/marmelab/react-admin/tree/master/packages/ra-data-simple-rest
"""

import json
from exxellence_shared_cga.core.types import (
    FilterOptions,
    RangeOptions,
    SemanticError,
    SemanticErrorType,
    SortOptions,
)
from fastapi import Query
from fastapi import status as http_status
from fastapi.exceptions import HTTPException
from pydantic import ValidationError
from typing import Optional, Type, TypeVar, Union

FieldsType = TypeVar("FieldsType", bound=Type)

filter_description = (
    "JSON object containing a filter of keys and values. Only rows matching "
    "all filters will be returned. "
    "Example: `{fieldX: filter_valueX, fieldY: filter_valueY}` "
    "Special filters are `{q: keyword}`, used to perform a full-text search"
)
range_description = (
    "Range of items to return. Should be a JSON list with two items: "
    "range start and range end (both inclusive, zero-based) "
    "Example: `[0, 9]`"
)
sort_description = (
    "sort; needs to be a JSON list with two elements: "
    "The field to sort by, and the order (ASC or DESC). "
    "Example: `[field, sortOrder]` where sortOrder is ASC or DESC"
)


class Config:
    extra = "forbid"


class SearchParameters:
    def __init__(
        self,
        filter_options_schema: Optional[FieldsType] = None,
        sort_options_schema: Optional[FieldsType] = None,
    ):
        self.filter_options_schema = filter_options_schema
        self.sort_options_schema = sort_options_schema

    def __call__(
        self,
        # `range` and `filter` are reserved words, so we use an alias. For symmetry, do the same
        # with sort.
        sort_: Optional[str] = Query(None, alias="sort", description=sort_description),
        range_: Optional[str] = Query(None, alias="range", description=range_description),
        filter_: Optional[str] = Query(None, alias="filter", description=filter_description),
    ) -> dict:
        return {
            "sort_options": self.parse_sort(sort_, self.sort_options_schema),
            "range_options": self.parse_range(range_),
            "filter_options": self.parse_filter(filter_, self.filter_options_schema),
        }

    def parse_filter(
        self, filter_: Optional[str], filter_options_schema: Optional[FieldsType] = None
    ) -> Union[FilterOptions, None]:
        if not filter_options_schema:
            raise ValueError("No filter schema. Add a filter schema to search_parameters call.")

        if not filter_:
            return None

        try:
            filter_dict = json.loads(filter_)
        except (json.JSONDecodeError, TypeError):
            raise HTTPException(
                http_status.HTTP_400_BAD_REQUEST, f"Invalid filter. {filter_description}"
            )

        try:
            filter_dict = filter_options_schema(**filter_dict).model_dump(exclude_unset=True)
        except ValidationError as e:
            msg = e.errors()
            msg.append(filter_options_schema.model_json_schema())
            raise SemanticError(
                loc={"source": "path", "field": "id"},
                msg=str(msg),
                type=SemanticErrorType.invalid_input,
            )

        return FilterOptions(options=filter_dict)

    def parse_range(self, range_: Optional[str]) -> Optional[RangeOptions]:
        if not range_:
            return None

        try:
            range_start, range_end = json.loads(range_)

        except json.JSONDecodeError as e:
            raise ValueError(f"Invalid range. {range_description}") from e

        return RangeOptions(
            start=range_start,
            end=range_end,
            check_bounds=self._is_check_range_bounds(),
        )

    def parse_sort(
        self, sort_: Optional[str], sort_options_schema: Optional[FieldsType] = None
    ) -> Optional[SortOptions]:
        if not sort_options_schema:
            raise ValueError("No sort schema. Add a sort schema to search_parameters call.")

        if not sort_:
            return None

        try:
            json_list = json.loads(sort_)
        except (json.JSONDecodeError, TypeError):
            raise HTTPException(
                http_status.HTTP_400_BAD_REQUEST,
                f"Invalid sort order: {sort_}. {sort_description}",
            )

        if len(json_list) != 2:
            raise HTTPException(
                http_status.HTTP_400_BAD_REQUEST,
                f"Invalid sort order: {sort_}. {sort_description}",
            )
        else:
            (field, order) = json_list

        # To sort on a join it is possible to add the model_name to the sort field. For example to
        # sort contact_moment on Employee add Employee.name use capital E on Employee
        model_name: Union[str, None] = None
        if field.count(".") == 1:
            model_name, field = field.split(".")

        sort_dict = {field: order}

        try:
            sort_options = sort_options_schema(**sort_dict).model_dump(exclude_unset=True)
        except ValidationError as e:
            msg = e.errors()
            msg.append(sort_options_schema.model_json_schema())
            raise SemanticError(
                loc={"source": "path", "field": "id"},
                msg=str(msg),
                type=SemanticErrorType.invalid_input,
            )

        field = list(sort_options.keys())[0]
        order = sort_options[field]

        return SortOptions(field=field, order=order, modelName=model_name)

    def _is_check_range_bounds(self) -> bool:
        return True


class LenientSearchParameters(SearchParameters):
    def _is_check_range_bounds(self) -> bool:
        return False
