# SPDX-FileCopyrightText: 2021 Exxellence
#
# SPDX-License-Identifier: EUPL-1.2

from fastapi import Request, status
from fastapi.responses import JSONResponse
from starlette.middleware.base import BaseHTTPMiddleware


class CheckContentTypeMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next):
        if request.method in {"POST", "PUT", "PATCH", "DELETE"}:
            content_type = request.headers.get("Content-Type")
            if content_type != "application/json" and content_type is not None:
                return JSONResponse(
                    status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
                    content={
                        "detail": [
                            {
                                "loc": {"source": "header", "field": "Content-Type"},
                                "msg": "Content-Type header must be application/json",
                                "type": "value_error.missing",
                            }
                        ]
                    },
                )

        return await call_next(request)
