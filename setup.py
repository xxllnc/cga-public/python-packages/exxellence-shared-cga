#!/usr/bin/env python

# SPDX-FileCopyrightText: Exxellence
#
# SPDX-License-Identifier: EUPL-1.2

# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import find_packages, setup  # type: ignore

with open("README.rst") as readme_file:
    readme = readme_file.read()

with open("requirements/base.txt") as f:
    requirements = f.read().splitlines()
    dependency_links = []
    for req in list(requirements):
        if req.startswith("git+"):
            requirements.remove(req)
            dependency_links.append(req)

with open("requirements/test.txt") as f:
    test_requirements = f.read().splitlines()

with open("requirements/dev.txt") as f:
    dev_requirements = f.read().splitlines()

requirements.append("typing; python_version<'3.5'")

setup(
    author="exxellence.shared.cga",
    author_email="a.boerma@exxellence.nl",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.12",
    ],
    description="Shared functions for Common Ground Apps",
    setup_requires=["pytest-runner"],
    install_requires=requirements,
    dependency_links=dependency_links,
    license="EUPL license",
    long_description=readme,
    include_package_data=True,
    keywords="exxellence_shared_cga",
    name="exxellence_shared_cga",
    packages=find_packages(include=["exxellence_shared_cga", "exxellence_shared_cga.*"]),
    package_data={"exxellence_shared_cga": ["py.typed"]},
    test_suite="tests",
    tests_require=test_requirements,
    extras_require={"dev": dev_requirements},
    url="https://gitlab.com/xxllnc/cga/python-packages/exxellence-shared-cga.git",
    version="0.1.35",
    zip_safe=False,
)
